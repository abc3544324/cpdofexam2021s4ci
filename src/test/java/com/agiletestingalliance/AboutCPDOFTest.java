package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

public class AboutCPDOFTest {
    @Test
    public void testdesc() throws Exception {

        String result= new AboutCPDOF().desc();
        assertTrue("Description doesn't contain Certification", result.contains("certification "));
        
    }
   
}
