package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

public class MinMaxTest {
    @Test
    public void testFFirst() throws Exception {

        int k= new MinMax().f(5,7);
        assertEquals("Incorrect", 7, k);
        
    }
	@Test
    public void testFSecond() throws Exception {

        int k= new MinMax().f(5,3);
        assertEquals("Incorrect", 5, k);
        
    }
   
    
}
